import unittest

class TestCaesarCipher(unittest.TestCase):
    def test_caesarCipher(self):
        # Test 1: Given test case
        s = "middle-Outz"
        k = 2
        expected = "okffng-Qwvb"
        result = caesarCipher(s, k)
        self.assertEqual(result, expected)

        # Test 2: Large shift value
        s = "middle-Outz"
        k = 100
        expected = "zrrgjr-Ihwj"
        result = caesarCipher(s, k)
        self.assertEqual(result, expected)
        
        # Test 3: Empty string
        s = ""
        k = 100
        expected = ""
        result = caesarCipher(s, k)
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
