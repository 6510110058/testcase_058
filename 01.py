import unittest

class TestFunnyString(unittest.TestCase):
    def test_funny_string(self):
        self.assertEqual(funnyString("acxz"), "Funny")
        self.assertEqual(funnyString("bcxz"), "Not Funny")
        self.assertEqual(funnyString("abcd"), "Not Funny")
        self.assertEqual(funnyString("abcdcba"), "Funny")
        self.assertEqual(funnyString("abcdefghijklmnopqrstuvwxyz"), "Not Funny")

if __name__ == '__main__':
    unittest.main()