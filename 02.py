import unittest

class TestAlternatingCharacters(unittest.TestCase):
    def test_alternating_characters(self):
        # Given
        s = "AAAA"
        # When
        result = alternatingCharacters(s)
        # Then
        self.assertEqual(result, 3)

    def test_alternating_characters_2(self):
        # Given
        s = "BBBBB"
        # When
        result = alternatingCharacters(s)
        # Then
        self.assertEqual(result, 4)

    def test_alternating_characters_3(self):
        # Given
        s = "ABABABAB"
        # When
        result = alternatingCharacters(s)
        # Then
        self.assertEqual(result, 0)

    def test_alternating_characters_4(self):
        # Given
        s = "BABABA"
        # When
        result = alternatingCharacters(s)
        # Then
        self.assertEqual(result, 0)

    def test_alternating_characters_5(self):
        # Given
        s = "AAABBB"
        # When
        result = alternatingCharacters(s)
        # Then
        self.assertEqual(result, 4)

if __name__ == '__main__':
    unittest.main()
